﻿using UnityEngine;
using System.Timers;
using System.Collections.Generic;

namespace ProjectClasses
{

    static public class CONSTANTS
    {
        public const float DEFAULT_ATK = (float)0.0;
        public const float DEFAULT_DEF = (float)0.0;
        public const float EVADE_ATK = (float)-100.0;
        public const float EVADE_DEF = (float)40.0;
        public const float ATTACKMOVE_ATK = (float)-10.0;
        public const float ATTACKMOVE_DEF = (float)20.0;
        public const float FOCUS_ATK = (float)50.0;
        public const float FOCUS_DEF = (float)-60.0;
        public const float MILLI = (float)1000.0;
    }

    public class Fleet
    {
        private List<Spaceship> ships;
        private Tactic tactic;
        private Vector3 location;
        private Vector3 destination;

        public Fleet(Vector3 location)
        {
            this.location = location;
            this.ships = new List<Spaceship>();
        }

        // add ships to the Fleet
        public void AddShips(List<Spaceship> shipsToAdd)
        {
            for (int i = 0; i < shipsToAdd.Count; i++)
            {
                this.ships.Add(shipsToAdd[i]);
            }
        }

        // add a single ship to the fleet
        public void AddShip(Spaceship shipToAdd)
        {
            this.ships.Add(shipToAdd);
        }

        // set next destination.
        public void SetDestination(Vector3 nextDestination)
        {
            this.destination = nextDestination;
        }

        // make one step.
        public void Move()
        {
            // the game is split into virtual squares.
            // each virtual-time-interval change, all the fleets are allowed to make one step towards the destination.
            Vector3 velocity;
            velocity = (this.destination - this.location);
            velocity = (float)(Fleet.max_valocity / velocity.magnitude) * velocity;
            this.location += velocity;
        }

        // check if the fleets are close enough to engage each other
        public bool CanEngage(Fleet other)
        {
            bool ans = false;
            double distance = Vector3.Distance(other.location, this.location);
            if (distance <= Fleet.max_engagement_distance)
            {
                ans = true;
            }
            return ans;
        }

        // starts an engagement with a hostile fleet if possible
        public void Engage(Fleet target)
        {
            ParseTactic(target);
            target.UpdateFleet();
        }

        // remove any destroyed ships
        private void UpdateFleet()
        {
            for (int i = 0; i < this.ships.Count; i++)
            {
                if (this.ships[i].Health <= 0)
                {
                    this.ships.Remove(this.ships[i]);
                }
            }
        }

        // parse tactic
        private void ParseTactic(Fleet enemyFleet)
        {
            Spaceship[] enemyShips = enemyFleet.ships.ToArray(); // easier access to the hostile fleet ships
            Spaceship[] friendlyShips = this.ships.ToArray();
            Spaceship ship;
            for (int i = 0; i < friendlyShips.Length; i++)
            {
                ship = friendlyShips[i];
                
                    if (i % 2 == 0)
                    {
                        ship.SetBehaviour(Spaceship.Behaviour.Focus);
                    }
                    else
                    {
                        ship.SetBehaviour(Spaceship.Behaviour.AttackMove);
                    }
                    ship.Engage(enemyShips[0]);
                
            }
        }

        // Getters
        public Vector3 Location
        {
            get
            {
                return this.location;
            }
        }
        public Vector3 Destination
        {
            get
            {
                return this.destination;
            }
        }
        public int FleetSize
        {
            get
            {
                return this.ships.Count;
            }
        }

        // the maximum engagement distance for two fleets.
        static public double max_engagement_distance = 10;
        // the maximum distance a fleet does in one step(Maximum velocity).
        static public double max_valocity = 2;
    }

    public class Battle
    {

    }

    public class Planet
    {

    }

    public class SuperWeapon
    {

    }

    public class Race
    {

    }

    public class Tactic
    {
        
    }

    public class Factory
    {

    }

    public class Spaceship
    {
        public enum Behaviour {Default, Evade, AttackMove, Focus};
        // Behaviour System Explaination //
        // bonus and penalty for the ship depending on its behaviour.
        // the behaviour system allow for giving each spaceship its own behaviour, besides the fleet orders.
        // <status>: <attack bonus/penalty>% <defence bonus/penalty>%
        // Default:      0%      0%  (spaceship is hovering and attacking, modifiers are not changed)
        // Evade:     -100%    +40%  (spaceship doing extreme manuevers, can not attack)
        // AttackMove: -10%    +20%  (spaceship is moving and shooting, harder to hit hostiles, harder for hostiles to hit)
        // Focus:      +50%    -50%  (spaceship is steady aiming, for perfect shots, much better aim, vulnurable to attacks)

        private string name;
        private Behaviour behaviour;
        private int health;
        private List<MountedWeapon> mountedWeapons;

        public Spaceship(string name, int health, List<MountedWeapon> mountedWeapons)
        {
            this.name = name;
            this.health = health;
            this.mountedWeapons = new List<MountedWeapon>(mountedWeapons);
            this.behaviour = Behaviour.Default;
        }
        
        // target another ship. for now, all of the weapons on the attacking ship attack the target
        public void Engage(Spaceship target)
        {
            
        }

        // checks wether any weapon on the ship is reloaded
        public bool IsAnyMountedWeaponReloaded()
        {
            bool ans = false;
            for (int i = 0; i < this.mountedWeapons.Count; i++)
            {
                if (this.mountedWeapons[i].IsReloaded())
                {
                    ans = true;
                }
            }
            return ans;
        }

        // get hit by a ship-mounted-weapon
        private void GetHit(MountedWeapon weapon)
        {
            this.health -= weapon.Damage;
        }

        // easier function to change behaviour
        public void SetBehaviour(Behaviour b)
        {
            this.behaviour = b;
        }

        // the functions below change the status of the spaceship.
        public void Default()
        {
            this.behaviour = Behaviour.Default;
        }
        public void Evade()
        {
            this.behaviour = Behaviour.Evade;
        }
        public void AttackMove()
        {
            this.behaviour = Behaviour.AttackMove;
        }
        public void Focus()
        {
            this.behaviour = Behaviour.Focus;
        }

        // these functions return the attack or defence modifiers of the status
        static public float GetAttackModifier(Behaviour behaviour)
        {
            switch (behaviour)
            {
                case (Behaviour.Evade):
                    return CONSTANTS.EVADE_ATK;
                case (Behaviour.AttackMove):
                    return CONSTANTS.ATTACKMOVE_ATK;
                case (Behaviour.Focus):
                    return CONSTANTS.FOCUS_ATK;
                default:
                    return CONSTANTS.DEFAULT_ATK;
            }
        }
        static public float GetDefenceModifier(Behaviour behaviour)
        {
            switch (behaviour)
            {
                case (Behaviour.Evade):
                    return CONSTANTS.EVADE_DEF;
                case (Behaviour.AttackMove):
                    return CONSTANTS.ATTACKMOVE_DEF;
                case (Behaviour.Focus):
                    return CONSTANTS.FOCUS_DEF;
                default:
                    return CONSTANTS.DEFAULT_DEF;
            }
        }

        // Getters
        public Behaviour ShipBehaviour
        {
            get
            {
                return this.behaviour;
            }
        }
        public int Health
        {
            get
            {
                return this.health;
            }
        }
    }

    public class MountedWeapon
    {
        private string name;
        private int accuracy;
        private int damage;
        private Timer reloader;

        public MountedWeapon(string name, float reloadTime, int accuracy, int damage)
        {
            this.name = name;
            this.accuracy = accuracy;
            this.damage = damage;
            this.reloader = new Timer(reloadTime * CONSTANTS.MILLI);

            // set the timer to work properly with 'delegate'
            this.reloader.Elapsed += delegate (object o, ElapsedEventArgs e)
            {
                this.reloader.Stop();
            };
        }

        // fire the weapon. the output is wether if the weapon hit the target or missed.
        public bool Fire(int carrierModifier, int targetModifier)
        {
            // the modifiers specify how to increase or decrease the accuracy of the weapon.
            // both modifiers are between -100% and 100%.
            bool isHit = false;
            int hit = this.accuracy + carrierModifier + targetModifier, output;
            // random value between 0(%) and 100(%).
            System.Random rand = new System.Random();
            output = rand.Next(0, 100);
            if (hit > output) // if the accuracy is bigger than the random value, it means the monution hits!
            {
                isHit = true;
            }
            this.reloader.Start();
            return isHit;
        }


        // checks if the weapon is ready to be used again
        public bool IsReloaded()
        {
            return this.reloader.Enabled == false;
        }

        // Getters
        public int Damage
        {
            get
            {
                return this.damage;
            }
        }
        public string Name
        {
            get
            {
                return this.name;
            }
        }
    }
}